#include "stack.hpp"

#include <utility>

using twist::MmapAllocation;
using twist::MemSpan;

namespace fiber {

FiberStack::FiberStack(MmapAllocation allocation)
    : allocation_(std::move(allocation)) {
}

FiberStack FiberStack::Allocate() {
  // todo: stack pooling

  static const size_t kNumPages = 4;  // 4KB stacks

  MmapAllocation allocation = MmapAllocation::AllocatePages(kNumPages);
  allocation.ProtectPages(/*offset=*/0, /*count=*/1);

  return FiberStack{std::move(allocation)};
}

char* FiberStack::Bottom() const {
  return (char*)((std::uintptr_t*)allocation_.End() - 1);
}

MemSpan FiberStack::AsMemSpan() const {
  return allocation_.AsMemSpan();
}

std::uintptr_t* FiberStack::LocalStorage() const {
  return (std::uintptr_t*)(allocation_.Start());
}

}  // namespace fiber

