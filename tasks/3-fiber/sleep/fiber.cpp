#include "fiber.hpp"

#include "scheduler.hpp"

#include <twist/support/compiler.hpp>
#include <twist/support/sequencer.hpp>

namespace fiber {

//////////////////////////////////////////////////////////////////////

static FiberId GenerateId() {
  static twist::Sequencer sequencer;
  return sequencer.Next();
}

Fiber* Fiber::Create(FiberRoutine routine) {
  auto* fiber = new Fiber();

  fiber->stack_ = FiberStack::Allocate();
  fiber->id_ = GenerateId();
  fiber->routine_ = std::move(routine);
  fiber->state_ = FiberState::Starting;

  SetupTrampoline(fiber);

  return fiber;
}

//////////////////////////////////////////////////////////////////////

static void FiberTrampoline() {
  Fiber* self = GetCurrentFiber();

  self->SetState(FiberState::Running);

  auto routine = self->UserRoutine();
  try {
    routine();
  } catch (...) {
    PANIC("Uncaught exception in fiber " << self->Id());
  }

  Terminate();  // never returns

  UNREACHABLE();
}

void Fiber::SetupTrampoline(Fiber* fiber) {
  fiber->context_.Setup(
      /*stack=*/fiber->stack_.AsMemSpan(),
      /*trampoline=*/FiberTrampoline);
}

}  // namespace fiber
