#pragma once

#include "fiber.hpp"
#include "stack.hpp"

#include <chrono>
#include <queue>

namespace fiber {

//////////////////////////////////////////////////////////////////////

using TimePoint = std::chrono::time_point<std::chrono::steady_clock>;
using Duration = std::chrono::nanoseconds;

//////////////////////////////////////////////////////////////////////

using FiberQueue = IntrusiveList<Fiber>;

class Scheduler {
 public:
  Scheduler();

  void Run(FiberRoutine main);

  void Spawn(FiberRoutine routine);
  void Yield();
  void SleepFor(Duration delay);
  void Terminate();

 private:
  void RunLoop();

  // Context switch: current fiber -> scheduler
  void SwitchToScheduler();
  // Context switch: scheduler -> fiber
  void SwitchTo(Fiber* fiber);

  void Reschedule(Fiber* fiber);
  void Schedule(Fiber* fiber);

  Fiber* CreateFiber(FiberRoutine routine);
  void Destroy(Fiber* fiber);

 private:
  ExecutionContext context_;
  FiberQueue run_queue_;
};

//////////////////////////////////////////////////////////////////////

Fiber* GetCurrentFiber();

//////////////////////////////////////////////////////////////////////

void RunScheduler(FiberRoutine main);

void Spawn(FiberRoutine routine);
void Yield();
void SleepFor(Duration delay);
void Terminate();

}  // namespace fiber
