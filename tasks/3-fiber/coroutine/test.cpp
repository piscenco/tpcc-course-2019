#include "coroutine.hpp"
#include "fiber.hpp"

#include <twist/test_framework/test_framework.hpp>

#include <memory>

struct TreeNode;

using TreeNodePtr = std::shared_ptr<TreeNode>;

struct TreeNode {
  TreeNodePtr left_;
  TreeNodePtr right_;

  TreeNode(TreeNodePtr left = nullptr, TreeNodePtr right = nullptr)
    : left_(std::move(left)), right_(std::move(right)) {
  }

  static TreeNodePtr Create(TreeNodePtr left, TreeNodePtr right) {
    return std::make_shared<TreeNode>(std::move(left), std::move(right));
  }

  static TreeNodePtr CreateLeaf() {
    return std::make_shared<TreeNode>();
  }
};


TEST_SUITE(Coroutine) {
  SIMPLE_TEST(JustWorks) {
    auto foo_routine = [&]() {
      coro::Suspend();
    };

    coro::Coroutine foo(foo_routine);

    ASSERT_FALSE(foo.IsCompleted());
    foo.Resume();
    foo.Resume();
    ASSERT_TRUE(foo.IsCompleted());

    ASSERT_THROW(foo.Resume(), coro::CoroutineCompleted)
  }

  SIMPLE_TEST(Interleaving) {
    int step = 0;

    auto finn_routine = [&]() {
      ASSERT_EQ(step, 0);
      step = 1;
      coro::Suspend();
      ASSERT_EQ(step, 2);
      step = 3;
    };

    auto jake_routine = [&]() {
      ASSERT_EQ(step, 1);
      step = 2;
      coro::Suspend();
      ASSERT_EQ(step, 3);
      step = 4;
    };

    coro::Coroutine finn(finn_routine);
    coro::Coroutine jake(jake_routine);

    finn.Resume();
    jake.Resume();

    ASSERT_EQ(step, 2);

    finn.Resume();
    jake.Resume();

    ASSERT_TRUE(finn.IsCompleted());
    ASSERT_TRUE(jake.IsCompleted());

    ASSERT_EQ(step, 4);
  }

  void TreeWalk(TreeNodePtr node) {
    if (node->left_) {
      TreeWalk(node->left_);
    }
    coro::Suspend();
    if (node->right_) {
      TreeWalk(node->right_);
    }
  }

  SIMPLE_TEST(TreeWalk) {
    TreeNodePtr root = TreeNode::Create(
      TreeNode::CreateLeaf(),
      TreeNode::Create(
        TreeNode::Create(
          TreeNode::CreateLeaf(),
          TreeNode::CreateLeaf()
        ),
        TreeNode::CreateLeaf()
      )
    );

    coro::Coroutine walker([&root]() {
      TreeWalk(root);
    });

    size_t node_count = 0;

    while (true) {
      walker.Resume();
      if (walker.IsCompleted()) {
        break;
      }
      ++node_count;
    }

    ASSERT_EQ(node_count, 7);
  }

  SIMPLE_TEST(Pipeline) {
    const size_t kSteps = 123;

    size_t inner_step_count = 0;

    auto inner_routine = [&]() {
      for (size_t i = 0; i < kSteps; ++i) {
        ++inner_step_count;
        coro::Suspend();
      }
    };

    auto outer_routine = [&]() {
      coro::Coroutine inner(inner_routine);
      while (!inner.IsCompleted()) {
        inner.Resume();
        coro::Suspend();
      }
    };

    coro::Coroutine outer(outer_routine);
    while (!outer.IsCompleted()) {
      outer.Resume();
    }

    ASSERT_EQ(inner_step_count, kSteps);
  }

  SIMPLE_TEST(NotInCoroutine) {
    ASSERT_THROW(coro::Suspend(), coro::NotInCoroutine)
  }

  SIMPLE_TEST(Exception) {
    auto foo_routine = [&]() {
      coro::Suspend();
      throw std::runtime_error("Test exception");
    };

    coro::Coroutine foo(foo_routine);

    ASSERT_FALSE(foo.IsCompleted());
    foo.Resume();
    ASSERT_THROW(foo.Resume(), std::runtime_error);
    ASSERT_TRUE(foo.IsCompleted());
  }

  struct MyException {
  };

  SIMPLE_TEST(NestedException1) {
    auto bar_routine = [&]() {
      throw MyException();
    };

    auto foo_routine = [&]() {
      coro::Coroutine bar(bar_routine);
      ASSERT_THROW(bar.Resume(), MyException);
    };

    coro::Coroutine foo(foo_routine);
    foo.Resume();
  }

  SIMPLE_TEST(NestedException2) {
    auto bar_routine = [&]() {
      throw MyException();
    };

    auto foo_routine = [&]() {
      coro::Coroutine bar(bar_routine);
      bar.Resume();
    };

    coro::Coroutine foo(foo_routine);
    ASSERT_THROW(foo.Resume(), MyException);
  }
}

TEST_SUITE(Fiber) {
  SIMPLE_TEST(PingPong) {
    int step = 0;

    auto finn = [&]() {
      ASSERT_EQ(step, 0);
      step = 1;
      fiber::Yield();
      ASSERT_EQ(step, 2);
      step = 3;
    };

    auto jake = [&]() {
      ASSERT_EQ(step, 1);
      step = 2;
      fiber::Yield();
      ASSERT_EQ(step, 3);
      step = 4;
    };

    fiber::RunScheduler([&]() {
      fiber::Spawn(finn);
      fiber::Spawn(jake);
    });

    ASSERT_EQ(step, 4);
  }

  class Baton {
   public:
    Baton(size_t count)
      : count_(count), current_(0) {
    }

    size_t CurrentOwner() {
      return current_;
    }

    void Transfer() {
      current_ = (current_ + 1) % count_;
    }

   private:
    const size_t count_;
    size_t current_;
  };

  SIMPLE_TEST(RelayRace) {
    const size_t kRunners = 10;
    const size_t kLoops = 10;

    Baton baton(kRunners);

    auto runner = [&](size_t t) {
      for (size_t i = 0; i < kLoops; ++i) {
        ASSERT_EQ(baton.CurrentOwner(), t);
        baton.Transfer();
        fiber::Yield();
      }
    };

    auto ref = [&]() {
      for (size_t i = 0; i < kRunners; ++i) {
        fiber::Spawn(std::bind(runner, i));
      }
    };

    fiber::RunScheduler(ref);

    ASSERT_EQ(baton.CurrentOwner(), 0);
  }
}

RUN_ALL_TESTS()
