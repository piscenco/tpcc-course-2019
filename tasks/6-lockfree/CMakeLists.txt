cmake_minimum_required(VERSION 3.9)

add_subdirectory(toyalloc)
add_subdirectory(stack)
add_subdirectory(logger)
add_subdirectory(queue-hazard)

