#include "toyalloc.hpp"

#include <twist/stdlike/atomic.hpp>
#include <twist/support/compiler.hpp>
#include <twist/support/locking.hpp>

using twist::MemSpan;
using twist::MmapAllocation;

namespace toyalloc {

static const size_t kBlockSize = 4096;

struct BlockNode {
  twist::atomic<BlockNode*> next_;
};

class Allocator {
 public:
  void Init(MmapAllocation arena) {
    arena_ = std::move(arena);
  }

  MemSpan GetArena() const {
    return arena_.AsMemSpan();
  }

  void* Allocate() {
    return nullptr;  // Not implemented
  }

  void Free(void* addr) {
    UNUSED(addr);
    // Not implemented
  }

 private:
  twist::MmapAllocation arena_;
};

/////////////////////////////////////////////////////////////////////

static Allocator allocator;

/////////////////////////////////////////////////////////////////////

void Init(MmapAllocation arena) {
  allocator.Init(std::move(arena));
}

MemSpan GetArena() {
  return allocator.GetArena();
}

size_t GetBlockSize() {
  return kBlockSize;
}

void* Allocate() {
  return allocator.Allocate();
}

void Free(void* addr) {
  allocator.Free(addr);
}

}  // namespace toyalloc
