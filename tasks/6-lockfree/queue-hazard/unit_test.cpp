#include <lfqueue.hpp>

#include <twist/test_framework/test_framework.hpp>
#include <twist/threading/test.hpp>

#include <string>

template <typename T>
using Queue = solutions::LockFreeQueue<T>;

TEST_SUITE(LockFreeQueue) {
  SIMPLE_T_TEST(EnqDeq) {
    Queue<std::string> q;
    q.Enqueue("Hello");
    std::string item;
    ASSERT_TRUE(q.Dequeue(item));
    ASSERT_EQ(item, "Hello");
    ASSERT_FALSE(q.Dequeue(item));
  }

  struct TestObject {
    static size_t ctor_count_;
    static size_t dtor_count_;

    TestObject() {
      ++ctor_count_;
    }

    TestObject(const TestObject& /*that*/) {
      ++ctor_count_;
    }

    TestObject(TestObject&& /*that*/) {
      ++ctor_count_;
    }

    TestObject& operator=(TestObject&& that) = default;
    TestObject& operator=(const TestObject& that) = default;

    ~TestObject() {
      ++dtor_count_;
    }

    static void ResetCounters() {
      ctor_count_ = 0;
      dtor_count_ = 0;
    }

    static void VerifyNoAliveObjects() {
      ASSERT_EQ(dtor_count_, ctor_count_);
    }
  };

  size_t TestObject::ctor_count_ = 0;
  size_t TestObject::dtor_count_ = 0;

  SIMPLE_T_TEST(EnqDeqRelease) {
    TestObject::ResetCounters();
    {
      Queue<TestObject> q;
      q.Enqueue(TestObject{});
      TestObject item;
      ASSERT_TRUE(q.Dequeue(item));
    }
    TestObject::VerifyNoAliveObjects();
  }

  SIMPLE_T_TEST(EnqRelease) {
    TestObject::ResetCounters();
    {
      Queue<TestObject> q;
      q.Enqueue(TestObject{});
    }
    TestObject::VerifyNoAliveObjects();
  }

  SIMPLE_T_TEST(FifoTest) {
    Queue<int> ints;
    static const int kEnq = 100;
    for (int i = 0; i < kEnq; ++i) {
      ints.Enqueue(i);
    }
    int next_int;
    for (int i = 0; i < kEnq; ++i) {
      ASSERT_TRUE(ints.Dequeue(next_int));
      ASSERT_EQ(next_int, i);
    }
    ASSERT_FALSE(ints.Dequeue(next_int));
  }
}

RUN_ALL_TESTS()
